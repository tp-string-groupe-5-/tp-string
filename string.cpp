#include "string.h"
#include <cstddef>
#include <iostream>

using std::cout;
using std::endl;

size_t string::MAX_SIZE=100; 

//--Constructors definitions--//

//default constructor
string::string(){
	size_=5;
	capacity_=10;
	data_ = new char[capacity_+1];
	char c[] = "Hello";
	for (size_t i=0; i<size_; ++i){
		data_[i] = c[i];
	}
	data_[size_] = '\0';
}


//c-string constructor
string::string(const char* s){
	size_=0;
	char i=s[size_];
	while(i!='\0'){
		++size_;
		i=s[size_];
	}
    if (size_ > MAX_SIZE){
        std::cout << "length ERROR -- string will not contain all the characters MAX SIZE exceed"<< std::endl;
        size_=MAX_SIZE;  
    }
    capacity_=2*size_;
    if(capacity_>MAX_SIZE){
        capacity_=MAX_SIZE;    
    }
	data_ = new char[capacity_+1];
	for (size_t i=0; i< size_; ++i){
		data_[i]=s[i];
	}
    data_[size_]='\0';

}

//Copy constructor
string::string(const string& str){
	capacity_= str.capacity_;
	size_ = str.size_;
	data_ = new char[str.capacity_];
	for(size_t i = 0; i < str.size_ + 1; ++i){
		data_[i] = str.data_[i];
	}
	data_[size_] = '\0';
}

//Destructor
string::~string(){
	delete [] data_;
}



//--getters--//
size_t string::length() const{
	return size_;
}

size_t string::max_size(){
	return MAX_SIZE;
}

size_t string::capacity() const{
	return capacity_;
}

//--print for test --//
void string::print(){
	int i=0;
	std::cout<< "size: " << size_ << std::endl;
    std::cout<<"capacity: "<<capacity_<<std::endl;
	std::cout<< "data: ";
	while(data_[i]!=0){
		std::cout << data_[i];
		++i;
	}
		std::cout<<std::endl;
}
//--setters--//
void string::resize(size_t n, char c){
	if (n>MAX_SIZE){
		std::cout << "length ERROR - sting unmodified" <<std::endl; 
	}else{
        if(n<size_){
            char temp[n+1];
            capacity_=n*2;
            for (size_t i=0; i< n; ++i){
		        temp[i]=data_[i];
	        }
            delete [] data_;
            data_=new char[capacity_+1];
            for (size_t i=0; i< n; ++i){
		        data_[i]=temp[i];
	        }
        }else if(n>capacity_){
            capacity_=n*2;
            if(capacity_>MAX_SIZE){
                capacity_=MAX_SIZE;
            }
	        char temp[size_+1];
            for (size_t i=0; i< size_; ++i){
		        temp[i]=data_[i];
	        }
            delete [] data_;
            data_=new char[capacity_+1];
            for (size_t i=0; i< size_; ++i){
		        data_[i]=temp[i];
	        }
            for (size_t i = size_; i < n; ++i){
			    data_[i]= c;
		    }
        }else{
            //(size_<n<capacity)
            for (size_t i = size_; i < n; ++i){
			    data_[i]= c;
		    }
        }
        data_[n]='\0';
        size_=n;
    }
}

void string::resize (size_t n){
	resize(n,'\0');
}


const char* string::c_str() const{
	return data_;
}


size_t string::size() const{
	return size_;
}

void string::clear(){
	int i=0;
	while(data_[i]!=0){
		data_[i]=0;
		++i;
	}
	size_ = 0;
}

void string::reserve(size_t n=0){
	if( n> capacity_){
		if(2*n < MAX_SIZE){
			capacity_ = 2*n;
		}
		else{
			capacity_ = MAX_SIZE;
		}
	}
	else{

		capacity_ = 2*n;
	}
}


//--Booleans--//

bool string::empty() const{
	if (size_==0){
		return 1;
	}
	else{
		return 0;
	}
}

//--Operators--//

string& string::operator= (const string& str){
    data_=str.data_;
    capacity_=str.capacity_;
    size_=str.size_;
    return *this;

}


string& string::operator= (char c){
	this->clear();
	this->resize(1);
	capacity_ = 2;
	data_[0] = c;
	return *this;
}


string operator+ (const string& lhs, char rhs){
    string res;
    delete [] res.data_;
    res.size_=lhs.size_+1;
    res.capacity_=res.size_*2;
    res.data_=new char[res.capacity_+1];
    for (size_t i=0; i<lhs.size_; ++i){
		res.data_[i]=lhs.data_[i];
	}
	res.data_[lhs.size_]=rhs; 
	return res;
}

string operator+ (const string& lhs, const char* rhs){
	string res;
	size_t size_rhs = 0;
	char c = rhs[size_rhs];
	while(c != '\0'){
		++size_rhs;
		c = rhs[size_rhs];
	}
	res.clear();
	res.resize(lhs.size_ + size_rhs);
	for (size_t i = 0; i < lhs.size_; ++i){
		res.data_[i]=lhs.data_[i];
	}
	size_t j = 0;
	for (size_t i = lhs.size_; i < res.size_; ++i){
		res.data_[i]=rhs[j];
		++j;
	}
	return res;
}

string operator+ (const string& lhs, const string& rhs){
    string res;
    delete [] res.data_;
    res.size_=lhs.size_ + rhs.size_+1;
    res.capacity_=res.size_;
    res.data_=new char[res.capacity_+1];
    for (size_t i=0; i<lhs.size_; ++i){
		res.data_[i]=lhs.data_[i];
	}
	res.data_[lhs.size_] = ' ';
	size_t i = lhs.size_ +1;
	for(size_t j = 0; j < rhs.size_; ++j){
		res.data_[i] = rhs.data_[j];
		++i;
		}
		res.data_[lhs.size_ + rhs.size_ + 1] = '\0';
	return res;
}











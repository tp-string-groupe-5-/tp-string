CXXFLAGS=-Wall -O0 -g -Wextra -std=c++11


main: main.o string.o
	g++ -o main main.o string.o

main.o: main.cpp string.h
	g++ -o main.o -c $(CXXFLAGS) main.cpp

string.o: string.cpp string.h
	g++ -o string.o -c $(CXXFLAGS) string.cpp

clean:
	rm main main.o string.o
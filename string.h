#ifndef STRING_
#define STRING_

#include <cstddef>

class string{
	friend string operator+ (const string& lhs, char rhs);
	friend string operator+ (const string& lhs, const char* rhs);
	friend string operator+ (const string& lhs, const string& rhs);

public :
	//---------------Constructors---------------//
	//default constructor
	string();

	//c-string constructor
	string(const char* s);
	/**
	Copies the null-terminated character sequence (C-string) pointed by s
	@param Pointer to an array of characters (such as a c-string)
	@return a string object
	*/
	
	//Copy constructor
	string(const string& str);
	/**
    Constructs a string object, copy of str

    @param str a string object
    @return a string object
	*/

	//---------------Destructor---------------//
	~string();
	/**
	Destroys the string object.
	This deallocates all the storage capacity allocated by the string using
	its allocator.
	@param none 
	@return none
	*/

	//---------------Member functions---------------//

	const char* c_str() const;

	/**
    Returns a pointer to an array that contains a null-terminated sequence of 
    characters(i.e., a C-string) representing the current value of the string 
    object. This array includes the same sequence of characters that make up 
    the value of the string object plus an additional 
    terminating null-character ('\0') at the end.
    @param none
    @return A pointer to the c-string representation of the string object's value
	*/

	size_t size() const;
	/**
    Returns the length of the string, in terms of bytes.
    @param none
    @return The number of bytes in the string
	*/
	
	void clear();
	/**
    Erases the contents of the string, which becomes an empty string (with a length of 0 characters).
    @param none
    @return none
	*/

	// Operator=
	//string (1);
	string& operator= (const string& str);
	/**
    Assigns a new value to the string, replacing its current contents
    by a character.
    @param A string object, whose value is either copied
    @return *this
	*/

	//c-string (2);
	string& operator= (const char* s);
	/**
    Assigns a new value to the string, replacing its current contents
    by a character.
    @param s A pointer to a null-terminated sequence of characters     
    @return *this
	*/

	//character (3)
	string& operator= (char c);
	/**
    Assigns a new value to the string, replacing its current contents
    by a character (the string length becomes 1).
    @param c A character     
    @return *this
	*/

	size_t length() const;
	/**
	Returns the length of the string, in terms of bytes.
	@param none
	@return The number of bytes in the string.
	*/
	
	void resize(size_t n);
	/**
	Resizes the string to a length of n characters.
	@param New string length, expressed in number of characters.
	@return none
	*/
	void resize(size_t n, char c);
	/**
	Resizes the string to a length of n characters.
	@param n New string length, expressed in number of characters.
	c Character used to fill the new character space added to the 
	string (in case the string is expanded).
	@return none
	*/

	bool empty() const;
	/**
	Returns whether the string is empty (i.e. whether its length is 0)
	@param none 
	@return true if the string length is 0, false otherwise
	*/

	void reserve(size_t n);
	/**
	Requests that the string capacity be adapted to a planned change in size to
	a length of up to n characters
	@param Planned length for the string.
	@return none
	*/

	static size_t max_size();
	/**
	Returns the maximum length the string can reach
	@param none 
	@return The maximum length the string can reach.
	*/

	size_t capacity() const;
	/**
	Returns the size of the storage space currently
	allocated for the string, expressed in terms of bytes.
	@param none 
	@return The size of the storage capacity currently allocated 
	for the string
	*/

	void print();

protected :
	// pointer on a char tab
	char* data_;

	// size in memory
	size_t size_;

	// maximum length of a string we can put in the tab
	size_t capacity_;

	// constant attribute, limit of capacity
	static size_t MAX_SIZE;


};

//---------------Non-member functions---------------//

// Operator+
	string operator+ (const string& lhs, char rhs);
	/**
    Returns a newly constructed string object with its value being the concatenation
    of the characters in lhs followed by those of rhs.
    @param Arguments to the left- and right-hand side of the operator, respectively
    @return A string
	*/

	string operator+ (const string& lhs, const char* rhs);
	/**
    Returns a newly constructed string object with its value being the concatenation
    of the characters in lhs followed by those of rhs. If of type char*, it shall 
    point to a null-terminated character sequence.
    @param Arguments to the left- and right-hand side of the operator, respectively
    @return A string
	*/
	string operator+ (const string& lhs, const string& rhs);
	/**
    Returns a newly constructed string object with its value being the concatenation
    of the characters in lhs followed by those of rhs.
    @param Arguments to the left- and right-hand side of the operator, respectively
    @return A string whose value is the concatenation of lhs and rhs.
	*/

#endif //STRING_



#include "string.h"
#include <iostream>

using std::cout;
using std::endl;

int main(){

	cout << "Hello World!" << endl;
	cout<<"------------------------" <<endl; 
	//default constructor
	cout<< "TEST default constructor" << endl; 
	string s;
	s.print();
	cout<<"------------------------" <<endl;

	//copy constructor
	cout<< "TEST copy constructor" << endl;
	string s1(s);
	s1.print();
	cout<<"------------------------" <<endl; 

	//c-string constructor 
	cout<< "TEST c-string constructor" << endl; 
	char c[] = "Hello World!";
	string s2(c);
	s2.print();
	char cata[30];
	string d(cata);
	d.print();
	cout<<"------------------------" <<endl; 
    cout << "TEST c-string > MAX SIZE" <<endl;
    char c2[]= "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
    string s22(c2);
    s22.print();
    cout<<"------------------------" <<endl;    
    
	//test of length
	cout << "Result of the test length() 1: " << (s.length()==5) << endl;
	cout << "Result of the test length() 2: " << (s.length()!=0) << endl; 
	cout<<"------------------------" <<endl; 

	//test of MAX_SIZE
	cout << "Result of the test max_size() 1: " << (s.max_size()==100) << endl;
	cout<<"------------------------" <<endl; 

	//test of resize (size_t, char)
	s.resize(10,'.');
	cout << "Result of the test resize(10,'.') : " << (s.length()==10) << endl; 
	s.print();
	cout<<"------------------------" <<endl; 
	s.resize(2,'.');
	cout << "Result of the test resize(2,'.') : " << (s.length()==2) << endl; 
	s.print();
	cout<<"------------------------" <<endl; 
	s.resize(100,'.');
	cout << "Result of the test resize(100,'.') : " << (s.length()==100) << endl; 
	s.print();
	cout<<"------------------------" <<endl; 
	s.resize(5,'3');
	cout << "Result of the test resize(5,'3') : " << (s.length()==5) << endl;
	s.print(); 
	cout<<"------------------------" <<endl; 
 	s.resize(200,'3');
	cout << "Result of the test resize(200,'3') : " << (s.length()!=100) << endl;
	s.print(); 
	cout<<"------------------------" <<endl;
	s.resize(-1,'3');
	cout << "Result of the test resize(-1,'3') : " << (s.length()!=100) << endl;
	s.print(); 
	cout<<"------------------------" <<endl;
    s.resize(0,'3');
	cout << "Result of the test resize(0,'3') : " << (s.length()==0) << endl;
	s.print(); 
	cout<<"------------------------" <<endl; 

	//test of resize(size_t)
	string s3;
	s3.resize(10);
	cout << "Result of the test resize(10) : " << (s3.length()==10) << endl; 
	s3.print();
	cout<<"------------------------" <<endl; 
	s3.resize(200);
	cout << "Result of the test resize(200) : " << (s3.length()==10) << endl; 
	s3.print();
	cout<<"------------------------" <<endl; 
    s3.resize(0);
	cout << "Result of the test resize(0) : " << (s3.length()==0) << endl;
	s3.print(); 
	cout<<"------------------------" <<endl;
	s3.resize(2);
	cout << "Result of the test resize(2) : " << (s3.length()==2) << endl; 
	s3.print();
	cout<<"------------------------" <<endl; 
	s3.resize(-1);
	cout << "Result of the test resize(-1) : " << (s3.length()==2) << endl;
	s3.print(); 
	cout<<"------------------------" <<endl;
 

	//Operator +
	string s4;
	cout << "String Hello + '.' : "<< endl;
	string s5 = s4+ '.';
	s5.print();
	cout<<"------------------------" <<endl; 
	cout << "String Hello 0 :" << endl;
	string s6 = s4 + '\0';
	s6.print();
	cout<<"------------------------" <<endl; 
	cout << "String Hello + ' ' : "<< endl;
	string s7 = s4 + ' ';
	s7.print();
	cout<<"------------------------" <<endl; 


	// cout << "//---------------Test print---------------//" << endl;
	// s.print();
	// s2.print();
	// cout << "//-----------------------------------------//" << endl;
	// cout << endl;

	cout << "//---------------Test c_str---------------//" << endl;
	//if output is 1, it means the address returned by c_str pointed on
	//the first letter of s and s2, which is H here
	
	//Test for each character inside the string
	cout << (*(s2.c_str()) == 'H') << endl; //adress of the first element
	// of the tab containing the data of the string
	cout << (*(s2.c_str()+1) == 'e') << endl;
	cout << (*(s2.c_str()+2) == 'l') << endl;
	cout << (*(s2.c_str()+3) == 'l') << endl;
	cout << (*(s2.c_str()+4) == 'o') << endl;
	cout << (*(s2.c_str()+5) == ' ') << endl;
	cout << (*(s2.c_str()+6) == 'W') << endl;
	cout << (*(s2.c_str()+7) == 'o') << endl;
	cout << (*(s2.c_str()+8) == 'r') << endl;
	cout << (*(s2.c_str()+9) == 'l') << endl;
	cout << (*(s2.c_str()+10) == 'd') << endl;
	cout << (*(s2.c_str()+11) == '!') << endl;
	cout << (*(s2.c_str()+12) == '\0') << endl;
	cout << "//-----------------------------------------//" << endl;
	cout << endl;


	cout << "//---------------Test size---------------//" << endl;
	s.print();
	cout << (s.size() == 100) << endl << endl;

	s1.print();
	cout << (s1.size() == 5) << endl << endl;

	s2.print();
	cout << (s2.size() == 12) << endl << endl;

	s3.print();
	cout << (s3.size() == 0) << endl << endl;

	s3.resize(200);
	cout << (s3.size()) << endl << endl;

	cout << "//-----------------------------------------//" << endl;
	cout << endl;


	cout << "//---------------Test size---------------//" << endl;
	char c24[] = "Hello World!";
	string s24(c24);
	s24.print();
	cout << (s24.size() == 12) << endl << endl;

	s24.resize(7);
	s24.print();
	cout << (s24.size() == 7) << endl << endl;

	s24.resize(12);
	s24.print();
	cout << (s24.size() == 12) << endl << endl;

	s24.resize(15);
	s24.print();
	cout << (s24.size() == 15) << endl << endl;

	string s25;
	s25.print();
	cout << (s25.size() == 5) << endl << endl;

	char c26[] = "";
	string s26(c26);
	s26.print();
	cout << (s26.size() == 0) << endl << endl;

	char c27[] = " ";
	string s27(c27);
	s27.print();
	cout << (s27.size() == 1) << endl << endl;

	char c28[] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	string s28(c28);
	s28.print();
	cout << (s28.size() == 100) << endl << endl;

	char c29[] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	string s29(c29);
	s29.print(); //c29 contains 101 times A but max capacity is 100
	// so s29.size() == 100
	cout << (s29.size() == 100) << endl << endl;

	char c30[] = "123456789";
	string s30(c30);
	string s31(s30);
	s31.print();
	cout << (s31.size() == 9) << endl << endl;


	cout << "//-----------------------------------------//" << endl;
	cout << endl;




	cout << "//---------------Test operator+---------------//" << endl;
    cout << "String Hello cata : "<< endl;
    string s8=s7+'c'+'a'+'t'+'a';
	s8.print();
	cout<<"------------------------" <<endl;
    cout<<"Test of the operator =:" <<endl;
    cout<<"1) a=b, a: "<<endl; 
    s4=s8;
    s4.print();
    cout<<"2) new b: "<<endl;
    s8=s7;
    s8.print();
    cout<<"3) a: "<<endl;
    s4.print();
    cout << "//-----------------------------------------//" << endl;
	cout << endl << endl;



	cout << "//---------------Test clear---------------//" << endl;
	char c32[] = "Hello World!";
	string s32(c32);
	s32.print();
    s32.clear();
    s32.print();

    char c33[] = "123456789";
	string s33(c33);
	s33.print();
    s33.clear();
    s33.print();

    char c34[] = "";
	string s34(c34);
	s34.print();
    s34.clear();
    s34.print();
    cout << "//-----------------------------------------//" << endl;
	cout << endl << endl;


	cout << "//---------------Test operator=(char)---------------//" << endl;
    char c35[] = "Hello World!";
	string s35(c35);
    char my_char = 'a';
    s35 = my_char;
    s35.print();

    char c36[] = "123456789";
	string s36(c36);
    char my_char2 = 'b';
    s36 = my_char2;
    s36.print();

    char c37[] = "";
	string s37(c37);
    char my_char3 = 'c';
    s37 = my_char3;
    s37.print();
    cout << "//-----------------------------------------//" << endl;
	cout << endl << endl;


	cout << "//---------------Test operator+(char*)---------------//" << endl;
    char c38[] = "Hello World!";
	string s38(c38);
    
    char c39[] = "123456789";

    string s40 = s38 + c39;
    s40.print();

    string s41 = s40 + c39;
    s41.print();

    char c42[] = "";
    string s42 = s38 + c42;
    s42.print();

    string s43(c42);
    s43 = s43 + c42;
    s43.print();
    cout << "//-----------------------------------------//" << endl;
	cout << endl << endl;

	//test of capacity//
	char cs[] = "Hello world!";
	string ss(cs);
	ss.print();
	cout << "The result of the test s5.capacity is: " << (ss.capacity()==2*ss.size()) << endl;
	cout << "----------------------------------" << endl;

	//test of empty() //
	char cs2[] = "";
	string ss2(cs2);
	ss2.print();
	cout<< "The result of the test ss2.empty is: " << (ss2.empty()==1) << endl;
	cout<<"------------------------" <<endl;

	//test of resize(size_t n=0)//

	char cs3[] = "Le fossé séparant théorie et pratique est moins large en théorie qu'il ne l'est en pratique";
	string ss3;
	ss3.print();
	size_t l = sizeof(cs3);
	ss3.resize(l);
	cout<< "The result of the test ss3.resize(size_t n) is: " << (ss3.capacity()==100) << endl;
	cout<<"------------------------" <<endl;
	
	//test of operator+(string)//
	char cs4[] = "J'entends le loup";
	char cs5[] = "le renard et la belette";
	string ss4(cs4);
	ss4.print();
	string ss5(cs5);
	ss5.print();
	string ss6 = ss4 + ss5;
	ss6.print();
	cout<<"------------------------" <<endl;


	return 0;
}

